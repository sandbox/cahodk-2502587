<?php

/**
 * @file
 * Provision/Drush hooks for the provision_tasks_extra module.
 *
 * These are the hooks that will be executed by the drush_invoke function.
 */

/**
 * Implementation of hook_drush_command().
 */
function provision_site_diff_drush_command() {
  $items['provision-snapshot'] = array(
    'description' => 'Saves a timestamped config snapshot of a site.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['provision-diff'] = array(
    'description' => 'Saves a diff of two snapshots.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Implements the provision-snapshot command.
 */
function drush_provision_site_diff_provision_snapshot() {

  // EMPTY
  
}

/**
 * Implements the provision-diff command.
 */
function drush_provision_site_diff_provision_diff() {

  // EMPTY
  
}

